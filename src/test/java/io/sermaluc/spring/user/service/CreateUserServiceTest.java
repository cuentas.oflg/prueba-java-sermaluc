package io.sermaluc.spring.user.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import io.sermaluc.spring.user.dto.CreateUserDto;
import io.sermaluc.spring.user.dto.CreateUsersPhonesDto;
import io.sermaluc.spring.user.repository.UsersPhonesRepository;
import io.sermaluc.spring.user.repository.UsersRepository;
import io.sermaluc.spring.user.utils.RestResponseException;

@ExtendWith(MockitoExtension.class)
public class CreateUserServiceTest {

    @Mock
    UsersRepository usersRepository;

    @Mock
    UsersPhonesRepository usersPhonesRepository;

    @InjectMocks
    CreateUserService createUserService;

    @Test
    @DisplayName("Test execute Happy Path")
    void test1() {
        var userId = UUID.randomUUID().toString();
        var email = "omar@dominio.cl";
        var phones = new CreateUsersPhonesDto(userId, "1234567", "1", "57");
        var createUserDto = new CreateUserDto("omar llaulle", email, "Pa$$w0rd", List.of(phones));
        when(usersRepository.findByEmail(email)).thenReturn(Optional.empty());
        assertEquals(userId, createUserService.execute(userId, createUserDto).getId());
    }

    @Test
    @DisplayName("Email Format Incorrect")
    void test2() {
        var userId = UUID.randomUUID().toString();
        var email = "omar_dominio.cl";
        var phones = new CreateUsersPhonesDto(userId, "1234567", "1", "57");
        var createUserDto = new CreateUserDto("omar llaulle", email, "Pa$$w0rd", List.of(phones));
        assertThrows(RestResponseException.class, () -> {
            createUserService.execute(userId, createUserDto);
        });
    }

    @Test
    @DisplayName("Password Format Incorrect")
    void test3() {
        var userId = UUID.randomUUID().toString();
        var email = "omar@dominio.cl";
        var phones = new CreateUsersPhonesDto(userId, "1234567", "1", "57");
        var createUserDto = new CreateUserDto("omar llaulle", email, "1234", List.of(phones));
        assertThrows(RestResponseException.class, () -> {
            createUserService.execute(userId, createUserDto);
        });
    }
}
