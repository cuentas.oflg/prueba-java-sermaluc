package io.sermaluc.spring.user.controller;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.sermaluc.spring.user.dto.CreateUserDto;
import io.sermaluc.spring.user.service.CreateUserService;
import io.sermaluc.spring.user.utils.RestResponse;

@RestController
public class CreateUserPostController {

    @Autowired
    private CreateUserService createCustomerService;

    @PostMapping("user/create")
    public ResponseEntity<RestResponse> index(@RequestBody CreateUserDto createUserDto) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(new RestResponse(true,
                        HttpStatus.CREATED.value(),
                        "",
                        createCustomerService.execute(UUID.randomUUID().toString(), createUserDto)));
    }
}
