package io.sermaluc.spring.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.sermaluc.spring.user.entity.UsersPhonesEntity;

@Repository
public interface UsersPhonesRepository extends JpaRepository<UsersPhonesEntity, String>{

}
