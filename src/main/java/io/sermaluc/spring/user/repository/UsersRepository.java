package io.sermaluc.spring.user.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.sermaluc.spring.user.entity.UsersEntity;

@Repository
public interface UsersRepository extends JpaRepository<UsersEntity, String>{

    Optional<UsersEntity> findByEmail(String email);

}
