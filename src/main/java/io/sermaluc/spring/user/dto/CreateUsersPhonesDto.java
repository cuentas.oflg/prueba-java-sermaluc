package io.sermaluc.spring.user.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Getter
@NoArgsConstructor
public class CreateUsersPhonesDto {
        private String userId;
        private String number;
        private String cityCode;
        private String countryCode;
}
