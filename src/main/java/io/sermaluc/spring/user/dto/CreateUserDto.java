package io.sermaluc.spring.user.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Getter
@NoArgsConstructor
public class CreateUserDto {
    private String name;
    private String email;
    private String password;
    private List<CreateUsersPhonesDto> phones;
}
