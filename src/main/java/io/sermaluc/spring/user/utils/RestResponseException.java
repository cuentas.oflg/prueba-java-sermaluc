package io.sermaluc.spring.user.utils;

import lombok.Getter;

@Getter
public class RestResponseException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public final boolean success;
	public final int statusCode;
	public final String message;
	public final Object data;

	public RestResponseException(boolean success, int statusCode,
			String message,
			Object data) {
		super(message);
		this.success = success;
		this.statusCode = statusCode;
		this.message = message;
		this.data = data;
	}
}