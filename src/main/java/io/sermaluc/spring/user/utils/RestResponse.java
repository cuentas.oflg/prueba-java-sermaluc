package io.sermaluc.spring.user.utils;

import java.io.Serializable;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class RestResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	public final boolean success;
	@JsonIgnore
	public final int statusCode;
	public final String message;
	public final Object data;

	public static ResponseEntity<RestResponse> list(List<? extends Object> list) {
		return ResponseEntity
				.status(HttpStatus.OK)
				.body(new RestResponse(true, 200, "", list));
	}
}
