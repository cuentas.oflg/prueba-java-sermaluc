package io.sermaluc.spring.user.utils;

import java.util.logging.Logger;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
	static final Logger LOG = Logger.getLogger(RestExceptionHandler.class.getName());

	@ExceptionHandler({ RestResponseException.class })
	public ResponseEntity<RestResponse> handleRestResponseException(Throwable ex, RestResponseException restResponseException) {
		return ResponseEntity
                .status(restResponseException.getStatusCode())
                .body(new RestResponse(restResponseException.success, restResponseException.getStatusCode(),
				restResponseException.getMessage(), restResponseException.getData()));
	}
}