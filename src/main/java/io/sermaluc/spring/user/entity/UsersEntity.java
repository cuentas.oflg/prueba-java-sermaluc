package io.sermaluc.spring.user.entity;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Table(name = "users")
@Entity(name = "users")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class UsersEntity implements Serializable {

    @Id
    @Column(name = "user_id", length = 36)
    private String userId;

    @Column(name = "name", length = 50, nullable = false, unique = false)
    private String name;

    @Column(name = "email", length = 50, nullable = false, unique = true)
    private String email;

    @Column(name = "password", length = 50, nullable = false, unique = false)
    private String password;

    @Column(name = "created", nullable = false, unique = false)
    private Date created;

    @Column(name = "modified", nullable = true, unique = false)
    private Date modified;

    @Column(name = "last_login", nullable = false, unique = false)
    private Date lastLogin;

    @Column(name = "token", nullable = false, unique = false)
    private String token;

    @Column(name = "is_active:", nullable = false, unique = false)
    private Boolean isActive;

}
