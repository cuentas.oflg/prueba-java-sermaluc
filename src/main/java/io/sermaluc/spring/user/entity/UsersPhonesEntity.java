package io.sermaluc.spring.user.entity;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Table(name = "users_phones")
@Entity(name = "users_phones")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class UsersPhonesEntity implements Serializable {

    @Id
    @Column(name = "user_phone_id", length = 36)
    private String userPhoneId;

    @Column(name = "user_id", length = 36, nullable = false)
    private String userId;

    @Column(name = "number", length = 50, nullable = false)
    private String number;

    @Column(name = "city_code", length = 50, nullable = false)
    private String cityCode;

    @Column(name = "country_code", length = 50, nullable = false)
    private String countryCode;

}
