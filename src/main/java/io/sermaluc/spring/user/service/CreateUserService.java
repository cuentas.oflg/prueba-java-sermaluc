package io.sermaluc.spring.user.service;

import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import io.sermaluc.spring.user.dto.CreateUserDto;
import io.sermaluc.spring.user.entity.UsersEntity;
import io.sermaluc.spring.user.entity.UsersPhonesEntity;
import io.sermaluc.spring.user.repository.UsersPhonesRepository;
import io.sermaluc.spring.user.repository.UsersRepository;
import io.sermaluc.spring.user.response.CreateUsersResponse;
import io.sermaluc.spring.user.utils.RestResponseException;

@Service
public class CreateUserService {

    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private UsersPhonesRepository usersPhonesRepository;

    public CreateUsersResponse execute(String userId, CreateUserDto createUserDto) {

        // Validar formato password
        if (!passwordValidate(createUserDto.getPassword())) {
            throw new RestResponseException(false, HttpStatus.NOT_ACCEPTABLE.value(),
                    "La clave no cumple con el criterio", null);
        }

        // Validar formato correo
        if (!emailValidate(createUserDto.getEmail())) {
            throw new RestResponseException(false, HttpStatus.NOT_ACCEPTABLE.value(),
                    "El correo no have el formato correcto", null);
        }
        // Validar si existe correo
        if (!usersRepository.findByEmail(createUserDto.getEmail()).isEmpty()) {
            throw new RestResponseException(false, HttpStatus.FOUND.value(),
                    "El correo ya fue registrado", null);
        }

        var date = new java.util.Date();
        var token = UUID.randomUUID().toString();

        // Persistir BD
        var usersEntity = new UsersEntity(userId, createUserDto.getName(),
                createUserDto.getEmail(), createUserDto.getPassword(), date, date , date, token, true);
        usersRepository.save(usersEntity);
        createUserDto.getPhones().stream().forEach(phone -> {
            var usersPhonesEntity = new UsersPhonesEntity(UUID.randomUUID().toString(), userId, phone.getNumber(),
                    phone.getCityCode(), phone.getCountryCode());
            usersPhonesRepository.save(usersPhonesEntity);
        });
        return new CreateUsersResponse(userId, date, date, date, token, true);
    }

    private boolean emailValidate(String email) {
        String expresionRegular = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
        Pattern patron = Pattern.compile(expresionRegular);
        Matcher matcher = patron.matcher(email);
        return matcher.matches();
    }

    public static boolean passwordValidate(String password) {
        int minLenght = 8;
        boolean haveUppercase = false;
        boolean haveLowercase = false;
        boolean haveNumber = false;
        boolean haveSpecialCharacter = false;

        if (password.length() < minLenght) {
            return false;
        }

        for (char caracter : password.toCharArray()) {
            if (Character.isUpperCase(caracter)) {
                haveUppercase = true;
            } else if (Character.isLowerCase(caracter)) {
                haveLowercase = true;
            } else if (Character.isDigit(caracter)) {
                haveNumber = true;
            } else {
                haveSpecialCharacter = true;
            }
        }

        return haveUppercase && haveLowercase && haveNumber && haveSpecialCharacter;
    }

}
