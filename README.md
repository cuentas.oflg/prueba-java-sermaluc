
# PRUEBA SERMALUC

El prueba consiste en crear una API de creación de usuarios

## Características técnicas de la prueba
 - [Java 17](https://www.oracle.com/java/technologies/javase/jdk17-archive-downloads.html)
 - [Maven](https://maven.apache.org/)
 - [Spring Boot 3.2.2](https://github.com/spring-projects/spring-boot/wiki/Spring-Boot-3.2-Release-Notes)
 - [BD H2](https://www.h2database.com/html/main.html)
 - [JPA](https://www.ibm.com/docs/es/was-liberty/nd?topic=liberty-java-persistence-api-jpa)

## Dependencias usadas
 - [Lombok](https://projectlombok.org/setup/maven)
 - [Junit](https://junit.org/junit5/docs/5.9.1/api/index.html)
 - [Mockito](https://site.mockito.org/)
 - [Spring Doc](https://springdoc.org/)

## Como levantar el proyecto
- Una vez ya descargado/clonado el repositorio e instalado el JDK 17, se procede a ejecutar (dar clic en iniciar) el proyecto desde el IDE que guste.
- La BD solo existe hasta finalizar la ejecución, todo se crea automaticamente (por las clases que terminan en Entity.java)
- Existe prueba unitaria al servicio (Buscar CreateUserServiceTest.java en el paquete test)
- Existe documentación, ingresar a la ruta http://{URL}:{PUERTO_SPRINGBOOT}/swagger-ui/index.html

## Consumo de la API
- Importar la siguiente secuencia al postman para consumir la API o en todo caso armarlo manualmente

curl --location 'localhost:8080/user/create' \
--header 'Content-Type: application/json' \
--header 'Cookie: JSESSIONID=24107E3E5CAB10605D54FF79595CADD9' \
--data-raw '{
    "name": "omar llaulle",
    "email": "omar2@dominio.cl",
    "password": "Pa$$w0rd",
    "phones": [
        {
            "number": "1234567",
            "cityCode": "1",
            "countryCode": "57"
        },
        {
            "number": "999999",
            "cityCode": "2",
            "countryCode": "58"
        }
    ]
}'

## Consideraciones
- Los IDs y el token son del tipo UUID
- Se adjunta diagrama con la explicación de los paquetes relevantes, la imagen se llama Diagrama.png
- La clave debe cumplir estás característica (está el método passwordValidate de la clase CreateUserService)
    - longitud minima - 8
    - Tener al menos 1 mayúscula
    - Tener al menos 1 minúscula
    - Tener al menos 1 número
    - Tener al menos 1 caracter especial